import { Component } from '@angular/core';
import { isNumber } from 'util';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  value='0';
  newValue=true;
  oldValue='';
  lastOperator = 'x';
  //numbers and operator array
  numbers = [
    ['C','√','x²','∛'],
    [7, 8, 9, 'x'],
    [4, 5, 6,'/'],
    [1, 2, 3,'+'],
    [0,'%','=','-'] 
  ];
  //other operators array
  operators = [
    ['sin'],
    ['cos'],
    ['tan'],
    ['log10'],
    ['ln']
  ];

  OnClick(operator){
    console.log(operator);
    if(isNumber(operator)){
      console.log('Es un número');
      if(this.newValue)
        this.value = '' + operator;
      else
        this.value += ''+ operator;
      this.newValue = false;
    }else if (operator === 'C'){
      this.value = '0';
      this.newValue = true;
    }else if (operator === '='){
      if(this.lastOperator === 'x'){
      this.value = ''+(parseInt(this.oldValue) * parseInt(this.value));
      }else if(this.lastOperator === '/'){
        this.value = ''+(parseInt(this.oldValue) / parseInt(this.value));
      }else if(this.lastOperator === '+'){
        this.value = ''+(parseInt(this.oldValue) + parseInt(this.value));
      }else if(this.lastOperator === '-'){
        this.value = ''+(parseInt(this.oldValue) - parseInt(this.value));
      }else if(this.lastOperator === '√'){
        this.value = ''+Math.sqrt(parseInt(this.value));
      }else if(this.lastOperator === '∛'){
        this.value = ''+Math.cbrt(parseInt(this.value));
      }else if(this.lastOperator === 'x²'){
        this.value = ''+ parseInt(this.value) * parseInt(this.value) ;
      }else if(this.lastOperator === '%'){
        this.value = ''+ parseInt(this.value) / 100 ;
      }else if(this.lastOperator === 'sin'){
        this.value = ''+Math.sin(parseInt(this.value));
      }else if(this.lastOperator === 'cos'){
        this.value = ''+Math.cos(parseInt(this.value));
      }else if(this.lastOperator === 'tan'){
        this.value = ''+Math.tan(parseInt(this.value));
      }else if(this.lastOperator === 'ln'){
        this.value = ''+Math.log(parseInt(this.value));
      }else if(this.lastOperator === 'log10'){
        this.value = ''+Math.log10(parseInt(this.value));
      }

      this.newValue = true;
    }else{
      this.newValue = true;
      this.oldValue = this.value;
      this.lastOperator = operator;     
    }
  }
  constructor() {}

}
